from django.db import models

class Account(models.Model):
    description = models.CharField(max_length=50, blank=False)
    account_number = models.CharField(max_length=10, blank=False)
    account_number_vd = models.CharField(max_length=2, blank=False)
    account_agency = models.CharField(max_length=5, blank=False)
    account_agency_vd = models.CharField(max_length=2, blank=False)
    bank = models.ForeignKey('Bank', on_delete=models.CASCADE, blank=False)
    aggrement_number = models.IntegerField(blank=False)