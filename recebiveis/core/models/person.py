from django.db import models

PERSON_TYPES = (('F', 'Física'), ('J', 'Jurídica'), )

class Address(models.Model):
    street_line1 = models.CharField(max_length=70)
    street_line2 = models.CharField(max_length=70)
    zip_code = models.CharField(max_length=10)
    city = models.ForeignKey('City', on_delete=models.CASCADE)

class Person(models.Model):
    name = models.CharField(max_length=70, blank=False)
    person_type = models.CharField(max_length=2, choices=PERSON_TYPES)
    documents = models.CharField(max_length=14, blank=False)
    address = models.ForeignKey('Address', null=True, on_delete=models.SET_NULL)
