from django.db import models

class Bank(models.Model):
    name = models.CharField(max_length=50)
    bank_number = models.IntegerField()