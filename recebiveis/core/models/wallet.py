from django.db import models

class Wallet(models.Model):
    description = models.CharField(max_length=50)
    receiving_rate = models.DecimalField(max_digits=5, decimal_places=2)
    account = models.ForeignKey('Account', on_delete=models.CASCADE)