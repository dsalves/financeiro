from django.db import models

class State(models.Model):
    initials = models.CharField(max_length=2, blank=False)
    name = models.CharField(max_length=50, blank=False)
    def __str__(self):
        return "{} - {}".format(self.initials, self.name)

class City(models.Model):
    name = models.CharField(max_length=50, blank=False)
    state = models.ForeignKey('State', on_delete=models.CASCADE, blank=False)
    def __str__(self):
        return "{} | {}".format(self.state.initials, self.name)