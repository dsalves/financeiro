from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^bank$', views.index_bank, name="index_bank"),
    url(r'^bank/new$', views.new_bank, name="new_bank"),

    url(r'^city$', views.index_city, name="index_city"),
    url(r'^city/new$', views.new_city, name="new_city"),

    url(r'^state$', views.index_state, name="index_state"),
    url(r'^state/new$', views.new_state, name="new_state"),
]
