from django import forms
from core.models import Bank, State, City

class BankForm(forms.ModelForm):
    class Meta:
        model = Bank
        fields = ('name', 'bank_number')

class StateForm(forms.ModelForm):
    class Meta:
        model = State
        fields = ('initials', 'name')

class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ('name', 'state')