from django.shortcuts import render, redirect
from django.urls import reverse
from core.forms import StateForm, CityForm
from core.models import City, State

def new_city(request):
    if request.method == "POST":
        form = CityForm(request.POST)
        if form.is_valid():
            city = form.save()
            return redirect(reverse('index_city'))
    else:
        form = CityForm()
    return render(request, 'core/city_new.html', {'form': form})


def edit_city(request):
    pass

def delete_city(request):
    pass

def index_city(request):
    cities = City.objects.all().order_by('state__initials', 'name')
    return render(request, 'core/city_index.html', {'cities': cities})


def new_state(request):
    if request.method == "POST":
        form = StateForm(request.POST)
        if form.is_valid():
            state = form.save()
            return redirect(reverse('index_state'))
    else:
        form = StateForm()
    return render(request, 'core/state_new.html', {'form': form})

def edit_state(request):
    pass

def delete_state(request):
    pass

def index_state(request):
    states = State.objects.all().order_by('initials')
    return render(request, 'core/state_index.html', {'states': states})