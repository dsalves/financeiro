from django.shortcuts import render, redirect
from django.urls import reverse
from core.forms import BankForm
from core.models import Bank

def new_bank(request):
    if request.method == "POST":
        form = BankForm(request.POST)
        if form.is_valid():
            bank = form.save()
            return redirect(reverse('index_bank'))
    else:
        form = BankForm()
    return render(request, 'core/bank_new.html', {'form': form})

def edit_bank(request):
    pass

def index_bank(request):
    all_banks = Bank.objects.all().order_by('bank_number')
    return render(request, 'core/bank_index.html', {'banks': all_banks})